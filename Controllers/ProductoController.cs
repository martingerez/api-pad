using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using webapi_pad_asp.Models;
using System.Linq;

namespace webapi_pad_asp.Controllers
{
    [Route("api/[controller]")]
    public class ProductoController : Controller
    {
        private readonly ProductoContext _context;

        public ProductoController(ProductoContext context)
        {
            _context = context;

            if (_context.Productos.Count() == 0)
            {
                _context.Productos.Add(new Producto { Nombre = "Producto1" });
                _context.SaveChanges();
            }
        }

        // GET: /api/producto - Devuelve todos los productos
        [HttpGet]
        public IEnumerable<Producto> GetAll()
        {
            return _context.Productos.ToList();
        }

        // GET: /api/producto/{id} - Devuelve el producto con el ID indicado
        [HttpGet("{id}", Name = "GetProducto")]
        public IActionResult GetById(long id)
        {
            var producto = _context.Productos.FirstOrDefault(t => t.Id == id);
            if (producto == null)
            {
                return NotFound();
            }
            return new ObjectResult(producto);
        }

        // POST: /api/producto
        [HttpPost]
        public IActionResult Create([FromBody] Producto producto)
        {
            if (producto == null)
            {
                return BadRequest();
            }

            _context.Productos.Add(producto);
            _context.SaveChanges();

            return CreatedAtRoute("GetProducto", new { id = producto.Id }, producto);
        }

        // PUT: /api/producto/{ID}
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Producto producto)
        {
            if (producto == null || producto.Id != id)
            {
                return BadRequest();
            }

            var prod = _context.Productos.FirstOrDefault(t => t.Id == id);
            if (prod == null)
            {
                return NotFound();
            }

            prod.Nombre = producto.Nombre;
            prod.Precio = producto.Precio;
            prod.Descripcion = producto.Descripcion;
            prod.Categoria = producto.Categoria;

            _context.Productos.Update(prod);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var prod = _context.Productos.FirstOrDefault(t => t.Id == id);
            if (prod == null)
            {
                return NotFound();
            }

            _context.Productos.Remove(prod);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}