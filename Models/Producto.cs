namespace webapi_pad_asp.Models
{
    public class Producto 
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public float Precio { get; set; }
        public string Categoria { get; set; }
    }
}