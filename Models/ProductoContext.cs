using Microsoft.EntityFrameworkCore;

// Clase encargada de la coordinacion entre Entity Framework y el modelo Producto
namespace webapi_pad_asp.Models
{
    public class ProductoContext : DbContext
    {
        public ProductoContext(DbContextOptions<ProductoContext> options)
            : base(options)
        {
        }

        public DbSet<Producto> Productos { get; set; }

    }
}